import { StyleSheet } from 'react-native';
export default StyleSheet.create({


  // ini style cari lokasi.js
  containerLokasi: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },


  rowLokasi: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },

  paddingRowLokasi: {
    padding: 40,
    alignItems: 'center'
  },


  // end cari lokasi.js

//////////////////////////////////
  //layout insert

  topImage: {
    width: 279,
    height: 156,
    borderRadius: 10
  },

  //end layout insert

  container: {
    flex: 1,
    flexDirection: "column",
    marginTop: 20
  },

  main_icon: {
    width: 350,
    height: 350
  },

  main_icon2: {
    width: 100,
    height: 100,
    margin: 0
  },

  menu_icon: {
    width: 75,
    height: 78,
  },

  row_icon: {
    justifyContent: 'space-around',
    flexDirection: 'row',
    marginTop: 30,
    width: '100%',
  },

  column_row: {
    flexDirection: 'column',
  },


  input: {
    padding: 10,
    height: 40,
    width: "80%",
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 10,
    marginTop: 15
  },

  btn_regis: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    width: '60%',
    height: 50,
    borderRadius: 12,
    backgroundColor: '#2EBC40'

  },

  btn_text: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold'
  },

  register: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: "#fff",
    alignItems: 'center',
  },
  txtCenter: {
    textAlign: 'center',
    padding: 30,
    fontSize: 30,
    fontWeight: 'bold',
    color: '#2EBC40'
  },

  txtCenterR: {
    textAlign: 'center',
    padding: 10,
    fontSize: 20,
    fontWeight: 'bold',
    color: '#2EBC40'
  },

  txtDeskripsi: {
    textAlign: 'center'
  },

  txtMenu: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#636363'
  },
  txt_login: {
    flex: 1,
    marginTop: 30,
    borderRadius: 12,
    flexDirection: 'column-reverse'

  },

});