import * as React from "react";
import {
  View,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  Text,
  Button,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet, StatusBar, AsyncStorage
} from "react-native";

import BottomTab from "./BottomTab";

const userInfo = { username: "admin", password: "pass" };

class LoginScreen extends React.Component {
  static navigateOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
  }

  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          alignItems: "center",
          marginTop: 30
        }}
      >
        <Image
          source={require("./assets/main_icon.png")}
          style={styles.main_icon}
        />

        

        <TextInput style={styles.input}
          placeholder="Username"
          onChangeText={username => this.setState({ username })}
          value={this.state.username}
          autoCapitalize="none"
        />

        <TextInput style={styles.input}
          placeholder="Password"
          secureTextEntry
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
        />

        <View style={styles.loginLayout}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate("Forgot");
            }}
          >
            <Text style={styles.text}>Lupa kata sandi?</Text>
          </TouchableOpacity>
        </View>

        
        <View style={styles.loginLayout}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate("Booking");
            }}
          >
            <Text style={styles.text}>Lupa kata sandi?</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.btn}>
          <TouchableOpacity
          onPress={this._login}
            // onPress={() => {
            //   this.props.navigation.navigate("Forgot");
            // }}
          >
            <Text style={styles.text_masuk}>Masuk</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.col}>
          <Text style={styles.text_akun}>Belum punya akun?</Text>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate("Register");
            }}
          >
            <Text style={styles.daftar}>Daftar</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  _login = async() => {
    if (userInfo.username === this.state.username && userInfo.password === this.state.password) {
      await AsyncStorage.setItem('isLoggedIn', '1');
      this.props.navigation.navigate("Dashboard");
    } else {
      alert('Username atau Password salah');
    }
    
  }
}

export default LoginScreen;

const styles = StyleSheet.create({
  loginLayout: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center"
  },

  col: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "stretch"
  },

  btn: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "green",
    width: "80%",
    borderRadius: 40,
    borderColor: "green",
    marginBottom: 40
  },

  text_masuk: {
    fontSize: 30,
    color: "white"
  },

  text: {
    flex: 1,
    padding: 10,
    fontSize: 20,
    color: "green",
    textAlign: "left",
    marginTop: 5,
    marginLeft: 31
  },

  input: {
    padding: 10,
    height: 40,
    width: "80%",
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 10,
    marginTop: 15
  },

  main_icon: {
    width: 350,
    height: 350
  },

  btn_masuk: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "green",
    borderWidth: 1,
    width: "80%",
    height: 90,
    borderRadius: 15,
    borderColor: "green",
    marginTop: 60
  },

  text_akun: {
    flex: 1,
    padding: 10,
    fontSize: 20,
    color: "grey",
    textAlign: "left",
    marginTop: 5,
    marginLeft: 20
  },

  daftar: {
    flex: 1,
    padding: 10,
    fontSize: 20,
    color: "green",
    textAlign: "right",
    marginTop: 5,
    marginRight: 20
  }
});
