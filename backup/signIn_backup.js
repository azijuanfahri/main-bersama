import * as React from "react";
import {
  View,
  Text,
  Button,
  StyleSheet,
  ScrollView,
  Image
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { MaterialIcons } from "@expo/vector-icons";



export default function SignInScreen(navigation) {
  return (
    <ScrollView>
      <Image
        source={require("./assets/main_icon.png")}
        style={styles.main_icon}
      />

      <View alignSelf="center">
        <TouchableOpacity style={styles.btn}>
          <MaterialIcons name="search" size={40} color="white"></MaterialIcons>
          <Text style={styles.text_btn}>Booking</Text>
        </TouchableOpacity>
      </View>

      <View>
        {/* ini tampilan lapangan */}
        <View style={styles.layout_lapangan}>
          <View style={styles.item_lapangan}>
            <Image
              source={require("./assets/lapangan_on.png")}
              style={styles.item_lapangan}
            />
            <Text style={styles.kota}>Jakata</Text>
          </View>

          <View style={styles.item_lapangan}>
            <Image
              source={require("./assets/lapangan_off.png")}
              style={styles.item_lapangan}
            />
            <Text style={styles.kota}>Surabaya</Text>
          </View>

          <View style={styles.item_lapangan}>
            <Image
              source={require("./assets/lapangan_off.png")}
              style={styles.item_lapangan}
            />
            <Text style={styles.kota}>Bandung</Text>
          </View>

          <View style={styles.item_lapangan}>
            <Image
              source={require("./assets/lapangan_off.png")}
              style={styles.item_lapangan}
            />
            <Text style={styles.kota}>Pekanbaru</Text>
          </View>

          <View style={styles.item_lapangan}>
            <Image
              source={require("./assets/lapangan_off.png")}
              style={styles.item_lapangan}
            />
            <Text style={styles.kota}>Semua kota</Text>
          </View>
        </View>

        {/* tampilan lapangan */}

        <View style={styles.jadwal}>
          <Text>Filter jadwal</Text>
        </View>

        {/* isi gambar Futsal */}
        <View>
          <ScrollView horizontal={true}>
            <View style={styles.descripsiLokasi}>
              <View>
                <Text style={styles.kategori}>Futsal</Text>
                <Image
                  source={require("./assets/futsal_vas.png")}
                  style={styles.item_gambar}
                />
              </View>

              <View flexDirection="row">
                <View flexDirection="column" flex="1">
                  <View flexDirection="row">
                    <MaterialIcons
                      name="date-range"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>30/03/2020</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="timer"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>14.00-16.00</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="location-on"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>Lapangan 1</Text>
                  </View>

                  <View flexDirection="row">
                    <TouchableOpacity style={styles.btn_detail}>
                      <Text style={styles.text_join}>Detail</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View flexDirection="column" flex="1" alignItems="center">
                  <MaterialIcons
                    name="account-circle"
                    size={60}
                    color="#2EBC40"
                    style={styles.icon_deskripsi}
                  ></MaterialIcons>
                  <Text style={styles.text_icon2}>10 orang</Text>

                  <TouchableOpacity style={styles.btn_join}>
                    <Text style={styles.text_join}>Join</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            {/* batas content */}
            <View style={styles.descripsiLokasi}>
              <View>
                <Text style={styles.kategori}>Futsal</Text>
                <Image
                  source={require("./assets/futsal_inter.png")}
                  style={styles.item_gambar}
                />
              </View>

              <View flexDirection="row">
                <View flexDirection="column" flex="1">
                  <View flexDirection="row">
                    <MaterialIcons
                      name="date-range"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>30/03/2020</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="timer"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>14.00-16.00</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="location-on"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>

                    <Text style={styles.text_icon}>Lapangan 1</Text>
                  </View>

                  <View flexDirection="row">
                    <TouchableOpacity style={styles.btn_detail}>
                      <Text style={styles.text_join}>Detail</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View flexDirection="column" flex="1" alignItems="center">
                  <MaterialIcons
                    name="account-circle"
                    size={60}
                    color="#D81F61"
                    style={styles.icon_deskripsi}
                  ></MaterialIcons>

                  <Text style={styles.text_icon_full}>15 orang</Text>

                  <TouchableOpacity style={styles.btn_join_full}>
                    <Text style={styles.text_join}>Full</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.item_gambar}>
              <Image
                source={require("./assets/futsal_semua.png")}
                style={styles.item_gambar}
              />
            </View>
          </ScrollView>
        </View>
        {/* isi futsal */}


                {/* BASKET */}
                <View>
          <ScrollView horizontal={true}>
            <View style={styles.descripsiLokasi}>
              <View>
                <Text style={styles.kategori}>Basket</Text>
                <Image
                  source={require("./assets/basket1.png")}
                  style={styles.item_gambar}
                />
              </View>

              <View flexDirection="row">
                <View flexDirection="column" flex="1">
                  <View flexDirection="row">
                    <MaterialIcons
                      name="date-range"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>30/03/2020</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="timer"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>14.00-16.00</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="location-on"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>Lapangan 1</Text>
                  </View>

                  <View flexDirection="row">
                    <TouchableOpacity style={styles.btn_detail}>
                      <Text style={styles.text_join}>Detail</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View flexDirection="column" flex="1" alignItems="center">
                  <MaterialIcons
                    name="account-circle"
                    size={60}
                    color="#2EBC40"
                    style={styles.icon_deskripsi}
                  ></MaterialIcons>
                  <Text style={styles.text_icon2}>10 orang</Text>

                  <TouchableOpacity style={styles.btn_join}>
                    <Text style={styles.text_join}>Join</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            {/* batas content */}
            <View style={styles.descripsiLokasi}>
              <View>
                <Text style={styles.kategori}>Basket</Text>
                <Image
                  source={require("./assets/basket2.png")}
                  style={styles.item_gambar}
                />
              </View>

              <View flexDirection="row">
                <View flexDirection="column" flex="1">
                  <View flexDirection="row">
                    <MaterialIcons
                      name="date-range"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>30/03/2020</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="timer"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>14.00-16.00</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="location-on"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>

                    <Text style={styles.text_icon}>Lapangan 1</Text>
                  </View>

                  <View flexDirection="row">
                    <TouchableOpacity style={styles.btn_detail}>
                      <Text style={styles.text_join}>Detail</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View flexDirection="column" flex="1" alignItems="center">
                  <MaterialIcons
                    name="account-circle"
                    size={60}
                    color="#D81F61"
                    style={styles.icon_deskripsi}
                  ></MaterialIcons>

                  <Text style={styles.text_icon_full}>15 orang</Text>

                  <TouchableOpacity style={styles.btn_join_full}>
                    <Text style={styles.text_join}>Full</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.item_gambar}>
              <Image
                source={require("./assets/futsal_semua.png")}
                style={styles.item_gambar}
              />
            </View>
          </ScrollView>
        </View>
        {/* BASKET */}

         {/* Sepak Bola */}
         <View>
          <ScrollView horizontal={true}>
            <View style={styles.descripsiLokasi}>
              <View>
                <Text style={styles.kategori}>Sepakbola</Text>
                <Image
                  source={require("./assets/sepakbola1.png")}
                  style={styles.item_gambar}
                />
              </View>

              <View flexDirection="row">
                <View flexDirection="column" flex="1">
                  <View flexDirection="row">
                    <MaterialIcons
                      name="date-range"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>30/03/2020</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="timer"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>14.00-16.00</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="location-on"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>Lapangan 1</Text>
                  </View>

                  <View flexDirection="row">
                    <TouchableOpacity style={styles.btn_detail}>
                      <Text style={styles.text_join}>Detail</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View flexDirection="column" flex="1" alignItems="center">
                  <MaterialIcons
                    name="account-circle"
                    size={60}
                    color="#2EBC40"
                    style={styles.icon_deskripsi}
                  ></MaterialIcons>
                  <Text style={styles.text_icon2}>10 orang</Text>

                  <TouchableOpacity style={styles.btn_join}>
                    <Text style={styles.text_join}>Join</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            {/* batas content */}
            <View style={styles.descripsiLokasi}>
              <View>
                <Text style={styles.kategori}>Sepakbola</Text>
                <Image
                  source={require("./assets/sepakbola2.png")}
                  style={styles.item_gambar}
                />
              </View>

              <View flexDirection="row">
                <View flexDirection="column" flex="1">
                  <View flexDirection="row">
                    <MaterialIcons
                      name="date-range"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>30/03/2020</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="timer"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>14.00-16.00</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="location-on"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>

                    <Text style={styles.text_icon}>Lapangan 1</Text>
                  </View>

                  <View flexDirection="row">
                    <TouchableOpacity style={styles.btn_detail}>
                      <Text style={styles.text_join}>Detail</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View flexDirection="column" flex="1" alignItems="center">
                  <MaterialIcons
                    name="account-circle"
                    size={60}
                    color="#2EBC40"
                    style={styles.icon_deskripsi}
                  ></MaterialIcons>

                  <Text style={styles.text_icon2}>15 orang</Text>

                  <TouchableOpacity style={styles.btn_join}>
                    <Text style={styles.text_join}>Join</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.item_gambar}>
              <Image
                source={require("./assets/futsal_semua.png")}
                style={styles.item_gambar}
              />
            </View>
          </ScrollView>
        </View>
        {/* SEPAKBOLA */}


        {/* VOLI*/}
        <View>
          <ScrollView horizontal={true}>
            <View style={styles.descripsiLokasi}>
              <View>
                <Text style={styles.kategori}>Voli</Text>
                <Image
                  source={require("./assets/voli1.png")}
                  style={styles.item_gambar}
                />
              </View>

              <View flexDirection="row">
                <View flexDirection="column" flex="1">
                  <View flexDirection="row">
                    <MaterialIcons
                      name="date-range"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>30/03/2020</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="timer"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>14.00-16.00</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="location-on"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>Lapangan 1</Text>
                  </View>

                  <View flexDirection="row">
                    <TouchableOpacity style={styles.btn_detail}>
                      <Text style={styles.text_join}>Detail</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View flexDirection="column" flex="1" alignItems="center">
                  <MaterialIcons
                    name="account-circle"
                    size={60}
                    color="#2EBC40"
                    style={styles.icon_deskripsi}
                  ></MaterialIcons>
                  <Text style={styles.text_icon2}>10 orang</Text>

                  <TouchableOpacity style={styles.btn_join}>
                    <Text style={styles.text_join}>Join</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            {/* batas content */}
            <View style={styles.descripsiLokasi}>
              <View>
                <Text style={styles.kategori}>Voli</Text>
                <Image
                  source={require("./assets/voli2.png")}
                  style={styles.item_gambar}
                />
              </View>

              <View flexDirection="row">
                <View flexDirection="column" flex="1">
                  <View flexDirection="row">
                    <MaterialIcons
                      name="date-range"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>30/03/2020</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="timer"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>
                    <Text style={styles.text_icon}>14.00-16.00</Text>
                  </View>

                  <View flexDirection="row">
                    <MaterialIcons
                      name="location-on"
                      size={28}
                      color="grey"
                      style={styles.icon_deskripsi}
                    ></MaterialIcons>

                    <Text style={styles.text_icon}>Lapangan 1</Text>
                  </View>

                  <View flexDirection="row">
                    <TouchableOpacity style={styles.btn_detail}>
                      <Text style={styles.text_join}>Detail</Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <View flexDirection="column" flex="1" alignItems="center">
                  <MaterialIcons
                    name="account-circle"
                    size={60}
                    color="#2EBC40"
                    style={styles.icon_deskripsi}
                  ></MaterialIcons>

                  <Text style={styles.text_icon2}>15 orang</Text>

                  <TouchableOpacity style={styles.btn_join}>
                    <Text style={styles.text_join}>Join</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.item_gambar}>
              <Image
                source={require("./assets/futsal_semua.png")}
                style={styles.item_gambar}
              />
            </View>
          </ScrollView>
        </View>
        {/* SEPAKBOLA */}

    
      </View>
    </ScrollView>
  );
}
// isi gambar

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  },

  item_lapangan: {
    marginTop: 10,
    width: 70,
    height: 50,
    marginHorizontal: 5,
    resizeMode: "contain"
  },

  kategori: {
    flex: 1,
    flexDirection: "row",
    marginLeft: 20,
    marginTop: 15,
    fontSize: 20,
    fontWeight: "bold"
  },

  item_gambar: {
    flex: 1,
    alignItems: "stretch",
    marginTop: 15,
    marginLeft: 10,
    width: 180,
    height: 150,
    borderRadius: 4,
    marginRight: 10
  },

  layout_item: {
    flex: 1,
    alignContent: "stretch"
  },

  descripsiLokasi: {
    flexDirection: "column"
  },

  icon_deskripsi: {
    marginLeft: 10,
    marginTop: 5
  },

  text_icon: {
    marginTop: 10,
    marginLeft: 5
  },
  text_icon2: {
    marginTop: 10,
    marginLeft: 5,
    color: "#2EBC40"
  },

  text_icon_full: {
    marginTop: 10,
    marginLeft: 5,
    color: "#D81F61"
  },

  kota: {
    textAlign: "center",
    fontSize: 14
  },

  layout_lapangan: {
    flexDirection: "row",
    justifyContent: "center"
  },

  jadwal: {
    flex: 1,
    flexDirection: "row",
    marginTop: 55,
    marginLeft: 20
  },

  main_icon: {
    alignSelf: "center",
    width: 500,
    height: 300,
    resizeMode: "contain"
  },

  btn: {
    alignItems: "center",
    marginTop: 10,
    flexDirection: "row",
    backgroundColor: "#2EBC40",
    borderRadius: 15,
    width: "100%",
    height: 60,
    justifyContent: "center"
  },

  btn_detail: {
    backgroundColor: "#2C87DC",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    marginTop: 10,
    marginLeft: 10,
    width: 60,
    height: 30
  },

  btn_join: {
    alignItems: "center",
    backgroundColor: "#2EBC40",
    borderRadius: 10,
    marginTop: 17,
    width: 60,
    height: 30,
    justifyContent: "center"
  },

  btn_join_full: {
    alignItems: "center",
    backgroundColor: "#D81F61",
    borderRadius: 10,
    marginTop: 17,
    width: 60,
    height: 30,
    justifyContent: "center"
  },

  text_join: {
    color: "white",
    fontSize: 15
  },

  text_btn: {
    color: "white",
    fontSize: 25
  }
});
