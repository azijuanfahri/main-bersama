import React, { Component } from "react";
import { StackActions } from "@react-navigation/native";
import axios from "axios";
import {
  View,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  AsyncStorage
} from "react-native";

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      token: ""
    };
  }

  handleUsername = username => {
    this.setState({ username: username });
  };

  handlePassword = password => {
    this.setState({ password: password });
  };

  storeData = async () => {
    try {
      await AsyncStorage.setItem("token", value);
    } catch (e) {
      // saving error
    }
  };

  handleLogin() {
    const username = this.state.username;
    const password = this.state.password;

    if (username === "" || password === "") {
      alert("Username dan Password tidak boleh kosong");
    } else {
      axios({
        method: "post",
        url: "https://mainbersama.demosanbercode.com/api/login",
        data: { email: username, password: password },
        config: {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
          }
        }
      })
        .then(function(response) {
          this.setState({ token: response.data.token });
          //this.props.navigation.dispatch(StackActions.replace("Dashboard"));
          //handle success
          // console.warn(response.data.token);
          this.setState({ token: response.data.token }); 
          this.storeData(respon.data.token);
          this.props.navigation.dispatch(StackActions.replace("Dashboard"));
        })
        .catch(function(error) {
          // console.warn(error);
          alert("login Gagal");
        });
        // ANJINGGGGG
    }
  }

  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.Os == "android" ? "padding" : "height"}
        style={styles.container}
      >
        <View style={styles.inner}>
          <Image
            source={require("../../assets/main_icon.png")}
            style={styles.main_icon}
          />

          <TextInput
            style={styles.input}
            placeholder="Email"
            onChangeText={username => this.handleUsername(username)}
            autoCapitalize="none"
          />

          <TextInput
            secureTextEntry={true}
            style={styles.input}
            placeholder="Password"
            onChangeText={password => this.handlePassword(password)}
            autoCapitalize="none"
          />

          <TouchableOpacity
            style={styles.loginLayout}
            onPress={() => {
              this.props.navigation.navigate("Forgot");
            }}
          >
            <Text style={styles.text}>Lupa kata sandi?</Text>
          </TouchableOpacity>

          {/* <TouchableOpacity
          style={styles.loginLayout}
          onPress={() => {
            this.props.navigation.navigate("Booking");
          }}
        >
          <Text style={styles.text}>Tes Menu Booking</Text>
        </TouchableOpacity> */}

          <View style={styles.btn_regis}>
            <TouchableOpacity onPress={() => this.handleLogin()}>
              <Text style={styles.btn_text}>Masuk</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.col}>
            <Text style={styles.text_akun}>Belum punya akun?</Text>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("Dashboard");
              }}
            >
              <Text style={styles.daftar}>Daftar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

//   _login = async () => {
//     if (
//       userInfo.username === this.state.username &&
//       userInfo.password === this.state.password
//     ) {
//       await AsyncStorage.setItem("isLoggedIn", "1");
//       this.props.navigation.navigate("Dashboard");
//     } else {
//       alert("Username atau Password salah");
//     }
//   };
// }

// export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },

  btn_regis: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: 30,
    width: "60%",
    height: 50,
    borderRadius: 12,
    backgroundColor: "#2EBC40"
  },

  btn_text: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold"
  },

  inner: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center"
  },

  header: {
    fontSize: 36,
    marginBottom: 48
  },
  textInput: {
    height: 40,
    borderColor: "#000000",
    borderBottomWidth: 1,
    marginBottom: 36
  },
  btnContainer: {
    backgroundColor: "white",
    marginTop: 12
  },
  loginLayout: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center"
  },

  col: {
    flexDirection: "row",
    alignSelf: "stretch"
  },

  btn: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "green",
    width: "80%",
    borderRadius: 40,
    borderColor: "green",
    marginBottom: 40
  },

  text_masuk: {
    fontSize: 30,
    color: "white"
  },

  text: {
    flex: 1,
    padding: 10,
    fontSize: 20,
    color: "green",
    textAlign: "left",
    marginTop: 5,
    marginLeft: 31
  },

  input: {
    padding: 10,
    height: 40,
    width: "80%",
    borderColor: "gray",
    borderWidth: 1,
    borderRadius: 10,
    marginTop: 15
  },

  main_icon: {
    width: 250,
    height: 250
  },

  btn_masuk: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "green",
    borderWidth: 1,
    width: "80%",
    height: 90,
    borderRadius: 15,
    borderColor: "green",
    marginTop: 60
  },

  text_akun: {
    flex: 1,
    padding: 10,
    fontSize: 20,
    color: "grey",
    textAlign: "left",
    marginTop: 5,
    marginLeft: 20
  },

  daftar: {
    flex: 1,
    padding: 10,
    fontSize: 20,
    color: "green",
    textAlign: "right",
    marginTop: 5,
    marginRight: 20
  }
});

export default LoginScreen;
