import React, { Component } from "react";
import { Image, View } from "react-native";

const ImagesExample = () => (
  <View style={{ flex: 1, padding: 10, alignSelf: "center" }}>
    <Image
      source={require(".../../assets/main_icon.png")}
      style={{ width: 250, height: 250 }}
    />
  </View>
);
export default ImagesExample;
