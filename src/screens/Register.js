import * as React from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Image
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import styles from "../../fashion/styles";

class Register extends React.Component {
  static navigateOption = {
    header: null
  };
  render() {
    return (
      <View>
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            alignItems: "center",
            marginTop: 30
          }}
        >
          <Image
            style={styles.main_icon2}
            source={require("../../assets/main_icon.png")}
          />

          <Text style={styles.txtCenterR}>Silahkan masukkan data</Text>

          <TextInput
            style={styles.input}
            placeholder="Username"
            autoCapitalize="none"
          />

          <TextInput
            style={styles.input}
            placeholder="Email"
            autoCapitalize="none"
          />

          <TextInput
            style={styles.input}
            placeholder="Sandi"
            autoCapitalize="none"
          />

          <View style={styles.btn_regis}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("Login");
              }}
            >
              <Text style={styles.btn_text}>Daftar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default Register;
