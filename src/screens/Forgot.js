import * as React from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
  AsyncStorage
} from "react-native";

import styles from "../../fashion/styles";

class LoginScreen extends React.Component {
  static navigateOptions = {
    header: null
  };

  render() {
    return (
      <View>
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            alignItems: "center",
            marginTop: 30
          }}
        >
          <Text style={styles.txtCenter}>Main Bersama</Text>

          <Text style={styles.txtDeskripsi}>
            {" "}
            Jangan khawatir, reset password cukup mudah. Hanya masukkan email
            saat kamu mendaftar di Main Bersama
          </Text>

          <TextInput
            style={styles.input}
            placeholder="Email"
            // onChangeText={username => this.setState({ username })}
            // value={this.state.username}
            autoCapitalize="none"
          />

          <View style={styles.btn_regis}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("Login");
              }}
            >
              <Text style={styles.btn_text}>Reset</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default LoginScreen;
