import { createStore } from "redux";
import AppReducers from "../reducers";

export default createStore(AppReducers);
