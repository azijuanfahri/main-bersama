import { combineReducers } from "redux";
//reducers
import authReducers from "./authReducers";

export default combineReducers({ authReducers });
