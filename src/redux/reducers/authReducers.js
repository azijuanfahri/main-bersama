const initialState = {
  isLoading: false,
  isLoaded: false,
  isError: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SAVE_TOKEN:
      return {
        ...state,
        token: action.payload
      };
    default:
      return state;
  }
};
