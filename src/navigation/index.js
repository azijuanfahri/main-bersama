import React from "react";
import { AsyncStorage } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import {
  LoginScreen,
  RegisterScreen,
  ForgotScreen,
  SignInScreen
} from "../screens";

const Stack = createStackNavigator();

export default props => {
  const check_token = async () => {
    let token = await AsyncStorage.getItem("token");
    console.warn(token);
    return token;
  };
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Register" component={RegisterScreen} />
        <Stack.Screen name="Forgot" component={ForgotScreen} />
        <Stack.Screen name="Dashboard" component={SignInScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
