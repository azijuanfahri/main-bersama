import * as React from "react";
import {
    View,
    Text,
    Image,
    TextInput,
    TouchableOpacity,
    StyleSheet, StatusBar, AsyncStorage
} from "react-native";

import styles from '../fashion/styles'
import { ScrollView } from "react-native-gesture-handler";


class LoginScreen extends React.Component {
    static navigateOptions = {
        header: null
    };



    render() {
        return (
            <ScrollView>
                <View style={styles.container}>

                    <View
                        style={styles.containerLokasi}
                    >


                        <Image style={styles.menu_icon}
                            source={require("../assets/location.png")}

                        />

                        <Text style={styles.txtMenu}>Bandung</Text>



                        <View style={styles.rowLokasi}>
                            <View style={styles.paddingRowLokasi}>
                                <Text style={styles.txtMenu}> Futsal 1 </Text>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("Insert");
                                    }}
                                >
                                    <Image style={styles.menu_icon}
                                        source={require("../assets/icon_voli.png")}
                                    />
                                </TouchableOpacity>

                                <Text> 5 Lapangan Tersedia   </Text>
                                <Text> 50.000/Jam   </Text>
                            </View>
                            <View style={styles.paddingRowLokasi}>
                                <Text style={styles.txtMenu}> Futsal 2 </Text>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("Insert");
                                    }}
                                >
                                    <Image style={styles.menu_icon}
                                        source={require("../assets/icon_voli.png")}
                                    />
                                </TouchableOpacity>

                                <Text> 5 Lapangan Tersedia   </Text>
                                <Text> 50.000/Jam   </Text>
                            </View>
                        </View>


                    </View>

                    <View
                        style={styles.containerLokasi}
                    >

                        <View style={styles.rowLokasi}>
                            <View style={styles.paddingRowLokasi}>
                                <Text style={styles.txtMenu}> Futsal 1 </Text>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("Insert");
                                    }}
                                >
                                    <Image style={styles.menu_icon}
                                        source={require("../assets/icon_voli.png")}
                                    />
                                </TouchableOpacity>
                                <Text> 5 Lapangan Tersedia   </Text>
                                <Text> 50.000/Jam   </Text>
                            </View>
                            
                            <View style={styles.paddingRowLokasi}>
                                <Text style={styles.txtMenu}> Futsal 2 </Text>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("Insert");
                                    }}
                                >
                                    <Image style={styles.menu_icon}
                                        source={require("../assets/icon_voli.png")}
                                    />
                                </TouchableOpacity>

                                <Text> 5 Lapangan Tersedia   </Text>
                                <Text> 50.000/Jam   </Text>
                            </View>
                        </View>


                    </View>

                    <View
                        style={styles.containerLokasi}
                    >

                        <View style={styles.rowLokasi}>
                            <View style={styles.paddingRowLokasi}>
                                <Text style={styles.txtMenu}> Futsal 1 </Text>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("Insert");
                                    }}
                                >
                                    <Image style={styles.menu_icon}
                                        source={require("../assets/icon_voli.png")}
                                    />
                                </TouchableOpacity>
                                <Text> 5 Lapangan Tersedia   </Text>
                                <Text> 50.000/Jam   </Text>
                            </View>
                            
                            <View style={styles.paddingRowLokasi}>
                                <Text style={styles.txtMenu}> Futsal 2 </Text>

                                <TouchableOpacity
                                    onPress={() => {
                                        this.props.navigation.navigate("Insert");
                                    }}
                                >
                                    <Image style={styles.menu_icon}
                                        source={require("../assets/icon_voli.png")}
                                    />
                                </TouchableOpacity>

                                <Text> 5 Lapangan Tersedia   </Text>
                                <Text> 50.000/Jam   </Text>
                            </View>
                        </View>


                    </View>


                </View>
            </ScrollView>
        );
    }


}

export default LoginScreen;
