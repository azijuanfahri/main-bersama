import * as React from "react";
import {
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  StyleSheet, StatusBar, AsyncStorage
} from "react-native";

import styles from '../fashion/styles';


class LoginScreen extends React.Component {
  static navigateOptions = {
    header: null
  };



  render() {
    return (
      <View>

  <View
        style={{
          flex: 1,
          flexDirection: "column",
          alignItems: "center",
          marginTop: 30
        }}
      >

              <Image style={styles.menu_icon}
          source={require("../assets/booking.png")}
          
        />

<View style={styles.row_icon}>
        <View flexDirection="column">
          <Image style={styles.menu_icon}
          source={require("../assets/icon_futsal_on.png")}
          
        />

        <Text style={styles.txtMenu}> Futsal </Text>

          </View>

          <View flexDirection="column">
          <Image style={styles.menu_icon}
          source={require("../assets/icon_basket.png")}
          
        />

        <Text style={styles.txtMenu}> Basket </Text>

          </View>

          <View flexDirection="column">
          <Image style={styles.menu_icon}
          source={require("../assets/icon_sepakbola.png")}
          
        />

        <Text style={styles.txtMenu}> Sepakbola </Text>

          </View>

          <View flexDirection="column">
          <Image style={styles.menu_icon}
          source={require("../assets/icon_voli.png")}
          
        />

        <Text style={styles.txtMenu}> Voli </Text>

          </View>

        </View>




      



      </View>

      <View style={{
   flex: 1,
   flexDirection: "column",
   alignItems: "center",
   marginTop: 220
      }}>
      <TextInput style={styles.input}
          placeholder="Masukkan Lokasi"
          // onChangeText={username => this.setState({ username })}
          // value={this.state.username}
          autoCapitalize="none"
        />

<TextInput style={styles.input}
          placeholder="Pilih Tanggal"
          // onChangeText={username => this.setState({ username })}
          // value={this.state.username}
          autoCapitalize="none"
        />


    

        <View style={styles.btn_regis}>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate("Lokasi");
            }}
          >
            <Text style={styles.btn_text}>Cari</Text>
          </TouchableOpacity>
        </View>
        </View>
      </View>
    );
  }


}

export default LoginScreen;
